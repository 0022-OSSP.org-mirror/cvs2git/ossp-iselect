##
##  .bashrc snippet for iSelect-based 'cd' command
##  Copyright (c) 1997 Ralf S. Engelschall, All Rights Reserved. 
##

#   database scan
cds () {
    (cd $HOME; 
     find . -type d -print |\
     sed -e "s;^\.;$HOME;" |\
     sort -u >$HOME/.dirs    ) &
}

#   enhanced cd command 
cd () {
    if [ -d $1 ]; then
         builtin cd $1
    else
         builtin cd `egrep "/$1[^/]*$" $HOME/.dirs |\
                     iselect -a -Q $1 -n "chdir" -t "Change Directory to..."` 
    fi
    PS1="\u@\h:$PWD\n:> "
}

#   make sure we stay at $HOME and 
#   that prompt gets initialized
cd

#   change to parent dir
alias -- -='cd ..'

#   change to last dir
alias .='cd $OLDPWD'

##EOF##
